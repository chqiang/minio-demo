package com.chq.minio.utils;

import io.minio.BucketExistsArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.PutObjectOptions;
import io.minio.errors.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @Description
 * @Author chenqiang
 * @Date 2020-11-18 12:32
 */
@Component
@Slf4j
public class MinIoUtils {

    @Autowired
    private MinioClient minioClient;

    /**
     * 判断 bucket是否存在
     * @param bucketName
     * @return
     */

    public boolean bucketExists(String bucketName) {
        try {
            return minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    /**
     * 创建 bucket
     * @param bucketName
     */

    public void makeBucket(String bucketName) {
        try {
            boolean isExist = minioClient.bucketExists(bucketName);
            if (!isExist) {
                minioClient.makeBucket(bucketName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 文件上传
     * @param bucketName
     * @param fileName
     * @param stream
     */

    public String putObject(String bucketName, String fileName, InputStream stream) {
        try {
            minioClient.putObject(bucketName, fileName, stream, new PutObjectOptions(stream.available(),-1));
            return getFileUrl(bucketName,fileName);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 删除文件
     *
     * @param bucketName
     * @param objectName
     */

    public void removeObject(String bucketName, String objectName) {

        try {
            minioClient.removeObject(bucketName, objectName);

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    /**
     * 获取下载地址
     * @param bucketName
     * @param fileName
     * @return
     */
    public String getFileUrl(String bucketName, String fileName) {
        try {
            return minioClient.presignedGetObject(bucketName, fileName);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void downloadFile(String bucketName, String fileName, String originalName, HttpServletResponse response) {
        try {

            InputStream file = minioClient.getObject(bucketName, fileName);
            String filename = new String(fileName.getBytes("ISO8859-1"), StandardCharsets.UTF_8);
            if (!StringUtils.isEmpty(originalName)) {
                fileName = originalName;
            }
            response.setHeader("Content-Disposition", "attachment;filename=" + filename);
            ServletOutputStream servletOutputStream = response.getOutputStream();

            int len;
            byte[] buffer = new byte[1024];
            while ((len = file.read(buffer)) > 0) {
                servletOutputStream.write(buffer, 0, len);
            }
            servletOutputStream.flush();
            file.close();
            servletOutputStream.close();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
