package com.chq.minio.utils;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @Description
 * @Author chenqiang
 * @Date 2020-11-18 14:19
 */
public class FileNameUtils {

    public static String getNextId (String suffix) {
        String fileName = "";
        // 生成6位数随机数
        // todo 算法需要优化
        String random = UUID.randomUUID().toString().replace("-", "");
        fileName = new StringBuilder().append(LocalDate.now().toString().replace("-", "")).append("/").append(random).append(suffix).toString();
        return fileName;
    }


    public static void main(String[] args) {
        String nextId = getNextId(".jpg");
        System.out.println(nextId);

    }
}
