package com.chq.minio.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chq.minio.entity.FileInfo;
import org.apache.ibatis.annotations.Mapper;
/**
 * @Description
 * @Author caiw
 * @Date 2021-01-07 11:19
 */
@Mapper
public interface FileInfoMapper extends BaseMapper<FileInfo> {
    int deleteByPrimaryKey(Long id);

    @Override
    int insert(FileInfo record);

    int insertSelective(FileInfo record);

    FileInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(FileInfo record);

    int updateByPrimaryKey(FileInfo record);
}