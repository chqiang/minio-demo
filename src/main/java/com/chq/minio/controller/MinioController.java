package com.chq.minio.controller;

import com.chq.minio.service.FileInfoService;
import com.chq.minio.utils.FileNameUtils;
import com.chq.minio.utils.MinIoUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @Description
 * @Author chenqiang
 * @Date 2020-11-18 11:19
 */
@RestController
@RequestMapping("/file")
@Slf4j
public class MinioController {

    @Value("${min.io.bucketName}")
    private String bucketName;

    @Autowired
    private MinIoUtils minIoUtils;

    @Autowired
    private FileInfoService fileInfoService;

    /**
     * 文件上传
     * @param file
     * @return
     */
    @PostMapping("/upload")
    public Object upload(@RequestParam("file") MultipartFile file) {
        boolean exists = minIoUtils.bucketExists(bucketName);
        if (!exists) {
            minIoUtils.makeBucket(bucketName);
            log.info("创建bucket");
        }
        try {
            // 获取文件前缀
            int index = file.getOriginalFilename().indexOf(".");
            //带点的后缀名
            String suffix = file.getOriginalFilename().substring(index);
            //不带点的后缀名
            String _suffix = file.getOriginalFilename().substring(index+1);
            // 文件名
            String fileName = FileNameUtils.getNextId(suffix);
            // 存储路径
            String storePath = fileName.substring(0,fileName.lastIndexOf("/")+1);
            // 表结构 id,文件类型，文件名字，存储路径
            String fileUrl = minIoUtils.putObject(bucketName, fileName, file.getInputStream());
            fileInfoService.createRecord(bucketName,fileName,_suffix,fileUrl,storePath);
            return fileUrl;
        } catch (IOException e) {
            log.error(e.getMessage());
            return "error";
        }
    }


    /**
     * 文件下载
     * @param fileName
     * @return
     */
    @GetMapping("/download")
    public void download(String fileName, HttpServletResponse response) {
        minIoUtils.downloadFile(bucketName, fileName, fileName, response);


    }


    /**
     * 文件删除
     * @param fileName
     * @return
     */
    @GetMapping("/delete")
    public ResponseEntity<Object> delete(String fileName) {
        HttpHeaders headers = new HttpHeaders();
        minIoUtils.removeObject(bucketName, fileName);
        boolean result = fileInfoService.deleteRecord(fileName);
        System.out.println(result);
        if (result){
            return new ResponseEntity("success", headers, HttpStatus.OK);
        }

        return new ResponseEntity("error", headers, HttpStatus.OK);
    }

    /**
     * 图片在线预览
     * @param fileName
     * @return
     */
    @GetMapping("/preview")
    public ResponseEntity<InputStreamResource> preview(String fileName) {
        //查询记录
        String urlName = fileInfoService.getUrl(fileName);
        if (Strings.isBlank(urlName)){
            return null;
        }
        URL url = null;
        InputStream inputStream = null;
        try {
            url = new URL(urlName);
            inputStream = url.openStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //返回图片
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        InputStreamResource inputStreamResource = new InputStreamResource(inputStream);
        return new ResponseEntity(inputStreamResource, headers, HttpStatus.OK);
    }

}
