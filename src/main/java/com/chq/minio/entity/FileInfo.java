package com.chq.minio.entity;

import java.util.Date;
/**
 * @Description
 * @Author caiw
 * @Date 2021-01-07 11:19
 */
public class FileInfo {
    private Long id;

    private String suffix;

    private String filename;

    private String storePath;

    private String fileUrl;

    private String bucketName;

    private Boolean delFlag;

    private Date createTime;

    public FileInfo(Long id, String suffix, String filename, String storePath, String fileUrl, String bucketName, Boolean delFlag, Date createTime) {
        this.id = id;
        this.suffix = suffix;
        this.filename = filename;
        this.storePath = storePath;
        this.fileUrl = fileUrl;
        this.bucketName = bucketName;
        this.delFlag = delFlag;
        this.createTime = createTime;
    }

    public FileInfo() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix == null ? null : suffix.trim();
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename == null ? null : filename.trim();
    }

    public String getStorePath() {
        return storePath;
    }

    public void setStorePath(String storePath) {
        this.storePath = storePath == null ? null : storePath.trim();
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl == null ? null : fileUrl.trim();
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName == null ? null : bucketName.trim();
    }

    public Boolean getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Boolean delFlag) {
        this.delFlag = delFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}