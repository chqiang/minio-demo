package com.chq.minio.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chq.minio.entity.FileInfo;
import com.chq.minio.mapper.FileInfoMapper;
import com.chq.minio.service.FileInfoService;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @Description
 * @Author caiw
 * @Date 2021-01-07 11:19
 */
@Service
public class FileInfoServiceImpl  extends ServiceImpl<FileInfoMapper, FileInfo> implements FileInfoService {

    @Autowired
    private FileInfoMapper fileInfoMapper;

    @Override
    public void createRecord(String bucketName, String fileName, String suffix, String fileUrl,String
                             storePath) {

        FileInfo record = new FileInfo();
        record.setBucketName(bucketName);
        record.setFilename(fileName);
        record.setSuffix(suffix);
        record.setFileUrl(fileUrl);
        record.setStorePath(storePath);
        record.setDelFlag(false);
        record.setCreateTime(new Date());
        fileInfoMapper.insert(record);

    }

    @Override
    public boolean deleteRecord(String fileName) {
        //删除记录
        UpdateWrapper<FileInfo> updateWrapper = new UpdateWrapper<FileInfo>();
        updateWrapper.eq("filename",fileName);
        updateWrapper.eq("del_flag",false);
        updateWrapper.set("del_flag",true);
        return this.update(updateWrapper);
    }

    @Override
    public String getUrl(String fileName) {

        QueryWrapper<FileInfo> queryWrapper = new QueryWrapper<FileInfo>();
        queryWrapper.eq("filename",fileName);
        queryWrapper.eq("del_flag",false);
        FileInfo fileInfo = this.getOne(queryWrapper);

        return fileInfo != null ? fileInfo.getFileUrl() : null;
    }
}
