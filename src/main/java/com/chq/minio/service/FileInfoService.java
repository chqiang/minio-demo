package com.chq.minio.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chq.minio.entity.FileInfo;
/**
 * @Description
 * @Author caiw
 * @Date 2021-01-07 11:19
 */
public interface FileInfoService extends IService<FileInfo> {
    public void createRecord(String bucketName,String fileName,String suffix,String fileUrl,String
            storePath);
    public boolean deleteRecord(String fileName);

    public String getUrl(String fileName);
}
