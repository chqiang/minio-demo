package com.chq.minio.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Description
 * @Author chenqiang
 * @Date 2020-11-18 11:49
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "min.io")
public class MinIoProperties {

    private String endpoint;

    private String accessKey;

    private String secretKey;
}
